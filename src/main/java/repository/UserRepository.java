package repository;

import domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by galik on 06.04.2016.
 */
public class UserRepository implements UserRepositoryInterface {

    private static List<User> db = new ArrayList<User>();

    @Override
    public User getApplicationByEmailAddress(String email) {
        for(User application: db) {
            if(application.getEmail().equalsIgnoreCase(email)) {
                return application;
            }
        }
        return null;
    }

    public void add(User application) {
        db.add(application);
    }

    public int count() {
        return db.size();
    }
}
