package repository;

import domain.User;

/**
 * Created by galik on 06.04.2016.
 */
public interface UserRepositoryInterface {

    User getApplicationByEmailAddress(String email);
    void add(User application);
    int count();
}
